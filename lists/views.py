from django.shortcuts import redirect, render
from django.http import HttpResponse
from lists.models import Address

def address_book(request):
    if request.method == 'POST':
        if(request.POST['name_text'].strip() != '' and request.POST['address_text'].strip() != ''):
            Address.objects.create(name=request.POST['name_text'], address=request.POST['address_text'])
            return redirect('/')

    addresses = Address.objects.all()
    return render(request, 'home.html', {'addresses': addresses})
