from django.test import TestCase
from django.urls import resolve
from lists.views import address_book
from lists.models import Address
from django.http import HttpRequest
from django.template.loader import render_to_string

class AddressBookTest(TestCase):
    
    def test_root_url_resolves_to_address_book_view(self):
        found = resolve('/')
        self.assertEqual(found.func, address_book)

    def test_address_book_returns_correct_html(self):
        response = self.client.get('/')
        html = response.content.decode('utf8')
        self.assertTrue(html.startswith('<html>'))
        self.assertIn('<title>Address Book</title>', html)
        self.assertTrue(html.strip().endswith('</html>'))

        self.assertTemplateUsed(response, 'home.html') 

    def test_uses_home_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'home.html')
    
    def test_can_save_a_POST_request(self):
        response = self.client.post('/', data={'name_text': 'A new address', 'address_text': '1234567890'})

        self.assertEqual(Address.objects.count(), 1)
        new_item = Address.objects.first()
        self.assertEqual(new_item.name, 'A new address')
        self.assertEqual(new_item.address, '1234567890')

    def test_redirects_after_POST(self):
        response = self.client.post('/', data={'name_text': 'A new address', 'address_text': '1234567890'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

    def test_only_saves_items_when_necessary(self):
        self.client.get('/')
        self.assertEqual(Address.objects.count(), 0)

    def test_displays_all_address(self):
        Address.objects.create(name='name 1', address='address 1')
        Address.objects.create(name='name 2', address='address 2')

        response = self.client.get('/')

        self.assertIn('name 1', response.content.decode())
        self.assertIn('address 1', response.content.decode())
        self.assertIn('name 2', response.content.decode())
        self.assertIn('address 2', response.content.decode())

class AddressModelTest(TestCase):

    def test_saving_and_retrieving_address(self):
        first_address = Address()
        first_address.name = 'first name'
        first_address.address = 'first address'
        first_address.save()

        second_address = Address()
        second_address.name = 'second name'
        second_address.address = 'second address'
        second_address.save()

        saved_address = Address.objects.all()
        self.assertEqual(saved_address.count(), 2)

        first_saved_address = saved_address[0]
        second_saved_address = saved_address[1]
        self.assertEqual(first_saved_address.name, 'first name')
        self.assertEqual(second_saved_address.name, 'second name')