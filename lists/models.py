from django.db import models

class Address(models.Model):
    name = models.TextField(default='')
    address = models.TextField(default='')
