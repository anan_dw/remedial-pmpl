# Remedial PMPL

Implement an Address Book

deploymeny on heroku: http://address-book-pmpl.herokuapp.com/

# Criteria Exercise 3
test isolation removing voodoo sleeps

Pada exercise 3 dilakukan test isolation yang bertujuan untuk menjalankan unit test tanpa mempengaruhi yang lainnya.
Django test runner membantu kita dengan membuat test database saat menjalanan test kemudian menghapus database tersebut 
sehingga ada sisa input dari testing.

Voodoo sleeps adalah sleep yang ditentukan secara eksplisit, tetapi durasi waktu untuk menunggu tidak dapat ditentukan dengan 
pasti karena banyak faktor yang mempengaruhi. Lebih baik jika menggunakan loop yang mengiterasi aplikasi kita secepat mungkin.