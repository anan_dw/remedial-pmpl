from selenium import webdriver
from django.test import LiveServerTestCase
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import WebDriverException
import time
import unittest

MAX_WAIT = 10


class NewVisitorTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def wait_for_row_in_list_table(self, row_text):
        start_time = time.time()
        while True:  
            try:
                table = self.browser.find_element_by_id('id_list_table')  
                rows = table.find_elements_by_tag_name('tr')
                self.assertIn(row_text, [row.text for row in rows])
                return  
            except (AssertionError, WebDriverException) as e:  
                if time.time() - start_time > MAX_WAIT:  
                    raise e  
                time.sleep(0.5)  

    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get(self.live_server_url)
        self.assertIn('Address Book', self.browser.title)

        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Address Book', header_text)

        name_inputbox = self.browser.find_element_by_id('id_new_address--name')
        self.assertEqual(
            name_inputbox.get_attribute('placeholder'),
            'Enter a name'
        )

        address_inputbox = self.browser.find_element_by_id('id_new_address--address')
        self.assertEqual(
            address_inputbox.get_attribute('placeholder'),
            'Enter an address'
        )

        name_inputbox.send_keys('dummy')
        address_inputbox.send_keys('1234567890')

        address_inputbox.send_keys(Keys.ENTER)

        self.wait_for_row_in_list_table('1: dummy - 1234567890')

        name_inputbox = self.browser.find_element_by_id('id_new_address--name')
        address_inputbox = self.browser.find_element_by_id('id_new_address--address')


        name_inputbox.send_keys('dummy2')
        address_inputbox.send_keys('0987654321')
        address_inputbox.send_keys(Keys.ENTER)

        self.wait_for_row_in_list_table('1: dummy - 1234567890')
        self.wait_for_row_in_list_table('2: dummy2 - 0987654321')
        

        self.fail('Finish the Test!')
        


